# RestApiFramework

My Framework is a combination of keyword driven framework wherein I have Test scrips ,Test cases.
My Framework can be divided into 6 major components - first is Test Driving Mechanism , second is my test scripts , third is common functions 
then i have data file , next is libraries and last one is reports.
In this framework's Test driving mechanism I have developed two Driving Mechanism :-

1)Static driver Class &
  2)Dynamic driver Class to drive my test executions.
  
* In'static driver class' we are statically calling the test script into main method and executing them,which is not advisible for that reason we have developed a new method called 'Dynamic driver class'.
* In which we can fetch the test cases to be executed from users from an Excel file & dynamically call the corresponding test  script on runtime, This is being done by using "Java.lang.reflect".
* Then we have common functions in which we have two categories first is "API Related Common Function" and second is "Utilities". 
In API Related Common Function we have the common functions which we use to trigger the API ,extract the responseBody and extract the response status code. 
* And then we have utilities section under the we have created utilities to create a directory when it does not exist  for test log files and if log directory exist we delete it and then recreate it. Then we have another utility which will automatically take the end point,requestBody,responseBody in the response headers and add it into a notepad file for each single test script.Then we have a more utility which will read the values from an Excel file.
* Then we are using Excel file to input the data to our framework or the request body paramaters are entered into an Excel file.Then we have libraries which we are using they are 
   1) RestAssured to triger the api ,extract the responebody and status code,
   2) JsonPath To parse the responsebody by using,
   3) Apache-Poi to read and write from an excel file, 
   4) TestNG libraries for validating responsebody parameters by using assert.
* In this project we are using  Allure reports and Extent reports.
