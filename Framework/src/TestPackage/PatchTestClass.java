package TestPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Api_Common_Methods.CommonMethodsHandleApi;
import Endpoints.PatchEndpoint;
import ReqRepository.PatchReqRepository;
import UtilityCommonMethods.ManageApiLogs;
import UtilityCommonMethods.ManageDirectory;
import io.restassured.path.json.JsonPath;

public class PatchTestClass extends CommonMethodsHandleApi {
	@Test
	public static void executor() throws IOException {
		File LogDir = ManageDirectory.CreateLogDirectory("PatchTestClassLogs");
		String requestbody = PatchReqRepository.PatchReqRepositoryTc1();
		String endpoint = PatchEndpoint.PatchEndpointTc1();
		for (int i = 0; i < 5; i++) {
			int statuscode = patch_statuscode(requestbody, endpoint);
			System.out.println(statuscode);

			if (statuscode == 200) {
				String responsebody = patch_responsebody(requestbody, endpoint);
				System.out.println(responsebody);
				ManageApiLogs.evidence_creator(LogDir,"PatchTestClass1", endpoint, requestbody, responsebody);
				PatchTestClass.validator(requestbody, responsebody);
				break;
			} else {
				System.out.println("Expected statuscode not found,hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String exp_date = currentdate.toString().substring(0, 8);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_date = jsp_res.getString("updatedAt").substring(0, 8);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, exp_date);

	}
}