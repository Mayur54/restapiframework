package ReqRepository;

import java.io.IOException;
import java.util.ArrayList;

import UtilityCommonMethods.ExcelDataExtractor;

public class PostReqRepository {
	public static String PostRequestTc1() throws IOException {
		ArrayList<String> data = ExcelDataExtractor.ExcelDataReader("test_data", "PostApi", "post_tc1");
		String name = data.get(1);
		String job = data.get(2);
		String requestbody = "{\r\n" + "    \"name\": \"" + name + "\",\r\n" + "    \"job\": \"" + job + "\"\r\n" + "}";
		return requestbody;

	}
}
