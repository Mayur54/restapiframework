package ReqRepository;

import java.io.IOException;
import java.util.ArrayList;

import UtilityCommonMethods.ExcelDataExtractor;

public class PutReqRepository {
	public static String PutReqRepositoryTc1() throws IOException {
		ArrayList<String> data = ExcelDataExtractor.ExcelDataReader("test_data","PutApi","put_tc3");
		String name = data.get(1);
		String job = data.get(2);
		String requestbody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		return requestbody;
	}

}
