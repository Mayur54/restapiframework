package ReqRepository;

import java.io.IOException;
import java.util.ArrayList;

import UtilityCommonMethods.ExcelDataExtractor;

public class PatchReqRepository {
	public static String PatchReqRepositoryTc1() throws IOException {
		ArrayList<String> data = ExcelDataExtractor.ExcelDataReader("test_data","PatchApi","patch_tc2");
		//System.out.println(data);
		String name = data.get(1);
		//System.out.println(name);
		String job = data.get(2);
		//System.out.println(job);
		String requestbody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		return requestbody;

	}

}
