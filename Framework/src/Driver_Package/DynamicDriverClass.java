package Driver_Package;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import UtilityCommonMethods.ExcelDataExtractor;

public class DynamicDriverClass {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> testcase_execute = ExcelDataExtractor.ExcelDataReader("test_data","Test_cases","TC_names");
		//System.out.println(testcase_execute);
		int count=testcase_execute.size();
		for (int i=1;i<count;i++)
		{
			String TC_names = testcase_execute.get(i);
			System.out.println(TC_names);
			// Step 1:Call the testcaseclass on routine by using java.lang.reflect package
			Class<?> TestClass = Class.forName("TestPackage."+TC_names);
			//Step 2:Call the execute method belonging to test class captured in variablr TC_names by using java.lang.reflect.method class
			Method ExecuteMethod = TestClass.getDeclaredMethod("executor");
			//Step 3:Set the accessibility of method true
			ExecuteMethod.setAccessible(true);
			//Step 4:Create the instance of testclass captured in variable name TC_names
			Object instanceOfTestClass = TestClass.getDeclaredConstructor().newInstance();
			//Step 5:Execute the test script class fetched in variable TestClass
			ExecuteMethod.invoke(instanceOfTestClass);
		}		 
	}
}
