package UtilityCommonMethods;

import java.io.File;

public class ManageDirectory {
	public static File CreateLogDirectory(String LogDir) {
		String ProjectDirectory = System.getProperty("user.dir");
		System.out.println("The current project directory path is : "+ProjectDirectory);
		File directory = new File(ProjectDirectory + "\\API_LOGS\\"+LogDir);
		
		if (directory.exists())
		{
			directory.delete();
			System.out.println(directory+" : is Deleted");
			directory.mkdir();
			System.out.println(directory+" : is Created");
		}
		else 
		{
			directory.mkdir();
			System.out.println(directory+" : is Created");
		}
		return directory;
	}

}
