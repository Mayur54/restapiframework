package UtilityCommonMethods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataExtractor {
	public static ArrayList<String> ExcelDataReader(String filename, String sheet_name, String testcase_name)
			throws IOException {
		ArrayList<String> Arraydata = new ArrayList<String>();
		String ProjectDirectory = System.getProperty("user.dir");
		// System.out.println(ProjectDirectory);
		// Step 1 :Create the object of file input stream to locate the data file
		FileInputStream fis = new FileInputStream(ProjectDirectory + "\\DataFile\\" + filename + ".xlsx");
		// Step 2 Create the XSSFWorkbook object to open the excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		// Step 3 : Fetch no. of sheets available in the excel file
		int count = wb.getNumberOfSheets();
		// System.out.println(count);
		// Step 4 : Access the sheet as per the input sheet name
		for (int i = 0; i < count; i++) {
			String sheetname = wb.getSheetName(i);
			// System.out.println(sheetname);
			if (sheetname.equals(sheet_name)) {
				System.out.println(sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row datarow = row.next();
					String testcasename = datarow.getCell(0).getStringCellValue();
					// System.out.println(testcasename);
					if (testcasename.equals(testcase_name)) {
						Iterator<Cell> cellvalues = datarow.iterator();
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							// System.out.println(testdata);
							Arraydata.add(testdata);
						}
					}
				}
				break;
			}
		}
		wb.close();
		return Arraydata;
	}
}
